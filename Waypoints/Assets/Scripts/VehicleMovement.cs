﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleMovement : MonoBehaviour
{
    public Transform goal;

    public float speed = 0;
    public float rotspeed = 3;
    public float acceleration = 5;
    public float decelereation = 5;
    public float minspeed = 0;
    public float maxspeed = 10;
    public float breakangle = 20;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        Vector3 lookatgoal = new Vector3(goal.transform.position.x,
                                           this.transform.position.y,
                                           goal.transform.position.z);

        Vector3 direction = lookatgoal - this.transform.position;

        this.transform.rotation = Quaternion.Slerp(this.transform.rotation,
                                                    Quaternion.LookRotation(direction),
                                                    Time.deltaTime * rotspeed);
        //speed = Mathf.Clamp(speed + (acceleration * Time.deltaTime), minspeed, maxspeed);

        if (Vector3.Angle(goal.forward, this.transform.forward) > breakangle && speed > 2)
        {
            speed = Mathf.Clamp(speed - (decelereation * Time.deltaTime), minspeed, maxspeed);
        }
        else
        {
            speed = Mathf.Clamp(speed + (acceleration * Time.deltaTime), minspeed, maxspeed);
        }

        this.transform.Translate(0, 0, speed);
    }
}
