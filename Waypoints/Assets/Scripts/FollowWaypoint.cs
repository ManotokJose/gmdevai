﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;

public class FollowWaypoint : MonoBehaviour
{
    public UnityStandardAssets.Utility.WaypointCircuit circuit;
    int currentwaypointindex = 0;

    float speed = 100;
    float rotspeed = 3;
    float accuracy = 1;
    // Start is called before the first frame update
    void Start()
    {
        //waypoints = GameObject.FindGameObjectsWithTag("Waypoint");
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (circuit.Waypoints.Length == 0)
        {
            return;
        }

        GameObject currentwaypoint = circuit.Waypoints[currentwaypointindex].gameObject;

        Vector3 lookatgoal = new Vector3(currentwaypoint.transform.position.x,
                                            this.transform.position.y,
                                            currentwaypoint.transform.position.z);

        Vector3 direction = lookatgoal - this.transform.position;

        if (direction.magnitude < 1.0f)
        {
            currentwaypointindex++;

            if (currentwaypointindex >= circuit.Waypoints.Length)
            {
                currentwaypointindex = 0;
            }
        }

        this.transform.rotation = Quaternion.Slerp(this.transform.rotation,
                                                    Quaternion.LookRotation(direction),
                                                    Time.deltaTime * rotspeed);

        this.transform.Translate(0, 0, speed * Time.deltaTime);
    }
}
