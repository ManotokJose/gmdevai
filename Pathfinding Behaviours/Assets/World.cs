﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class World : MonoBehaviour
{
    private static readonly World instance = new World();

    private static GameObject[] HidingSpots;

    static World()
    {
        HidingSpots = GameObject.FindGameObjectsWithTag("Hide");
    }

    private World() { }

    public static World Instance
    {
        get { return instance; }
    }

    public GameObject[] GetHidingSpots()
    {
        return HidingSpots;
    }
}
