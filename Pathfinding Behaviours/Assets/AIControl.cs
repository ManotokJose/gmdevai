﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.Purchasing;
using UnityEngine;
using UnityEngine.AI;

public class AIControl : MonoBehaviour
{
    NavMeshAgent agent;

    public GameObject target;

    public WASDMovement PlayerMovement;

    public int ID;

    // Start is called before the first frame update
    void Start()
    {
        agent = this.GetComponent<NavMeshAgent>();
        PlayerMovement = target.GetComponent<WASDMovement>();
    }

    void Seek(Vector3 location)
    {
        agent.SetDestination(location);
    }

    void Flee(Vector3 location)
    {
        Vector3 FleeDirection = location - this.transform.position;
        agent.SetDestination(this.transform.position - FleeDirection);
    }

    void Pursue()
    {
        Vector3 TargetDirection = target.transform.position - this.transform.position;
        
        float LookAhead = TargetDirection.magnitude / (agent.speed + PlayerMovement.currentSpeed);

        Seek(target.transform.position + target.transform.forward * LookAhead);
    }

    void Evade()
    {
        Vector3 TargetDirection = target.transform.position - this.transform.position;

        float LookAhead = TargetDirection.magnitude / (agent.speed + PlayerMovement.currentSpeed);

        Flee(target.transform.position + target.transform.forward * LookAhead);
    }

    Vector3 WanderTarget;

    void Wander()
    {
        float WanderRadius = 20;
        float WanderDistance = 10;
        float WanderJitter = 1;

        WanderTarget += new Vector3(Random.Range(-1.0f, 1.0f) * WanderJitter,
                                    0,
                                    Random.Range(-1.0f, 1.0f) * WanderJitter);
        WanderTarget.Normalize();
        WanderTarget *= WanderRadius;

        Vector3 TargetLocal = WanderTarget + new Vector3(0, 0, WanderDistance);
        Vector3 TargetWorld = this.gameObject.transform.InverseTransformVector(TargetLocal);

        Seek(TargetWorld);
    }

    void Hide()
    {
        float Distance = Mathf.Infinity;
        Vector3 ChosenSpot = Vector3.zero;

        int HidingSpotCount = World.Instance.GetHidingSpots().Length;

        for (int i = 0; i < HidingSpotCount; i++)
        {
            Vector3 HideDirection = World.Instance.GetHidingSpots()[i].transform.position - target.transform.position;
            Vector3 HidePosition = World.Instance.GetHidingSpots()[i].transform.position + HideDirection.normalized * 5;

            float SpotDistance = Vector3.Distance(this.transform.position, HidePosition);
            if (SpotDistance < Distance)
            {
                ChosenSpot = HidePosition;
                Distance = SpotDistance;
            }
        }

        Seek(ChosenSpot);
    }

    void CleverHide()
    {
        float Distance = Mathf.Infinity;
        Vector3 ChosenSpot = Vector3.zero;
        Vector3 ChosenDirection = Vector3.zero;
        GameObject ChosenGameObject = World.Instance.GetHidingSpots()[0];

        int HidingSpotCount = World.Instance.GetHidingSpots().Length;

        for (int i = 0; i < HidingSpotCount; i++)
        {
            Vector3 HideDirection = World.Instance.GetHidingSpots()[i].transform.position - target.transform.position;
            Vector3 HidePosition = World.Instance.GetHidingSpots()[i].transform.position + HideDirection.normalized * 5;

            float SpotDistance = Vector3.Distance(this.transform.position, HidePosition);
            if (SpotDistance < Distance)
            {
                ChosenSpot = HidePosition;
                ChosenDirection = HideDirection;
                ChosenGameObject = World.Instance.GetHidingSpots()[i];
                Distance = SpotDistance;
            }
        }

        Collider HideCol = ChosenGameObject.GetComponent<Collider>();
        Ray Back = new Ray(ChosenSpot, -ChosenDirection.normalized);
        RaycastHit info;
        float RayDistance = 100.0f;
        HideCol.Raycast(Back, out info, RayDistance);

        Seek(info.point + ChosenDirection.normalized * 5);
    }

    bool CanSeeTarget()
    {
        RaycastHit RaycastInfo;
        Vector3 RayToTarget = target.transform.position - this.transform.position;
        if (Physics.Raycast(this.transform.position, RayToTarget, out RaycastInfo))
        {
            return RaycastInfo.transform.gameObject.tag == "Player";
        }

        return false;
    }

    // Update is called once per frame
    void Update()
    {
        if (CanSeeTarget())
        {
            switch (ID)
            {
                case 0:
                    Pursue();
                    break;

                case 1:
                    Hide();
                    break;

                case 2:
                    Evade();
                    break;
            }
        }
        else
        {
            Wander();
        }
    }
}
