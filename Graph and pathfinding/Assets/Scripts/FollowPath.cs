﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPath : MonoBehaviour
{
    Transform goal;
    float speed = 5.0f;
    float accuracy = 1.0f;
    float rotspeed = 2.0f;
    public GameObject wpManager;
    GameObject[] wps;
    GameObject currentNode;
    int currentwpindex = 0;
    Graph graph;
    // Start is called before the first frame update
    void Start()
    {
        wps = wpManager.GetComponent<WaypointManager>().waypoints;
        graph = wpManager.GetComponent<WaypointManager>().graph;
        currentNode = wps[0];
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (graph.getPathLength() == 0 || currentwpindex == graph.getPathLength())
        {
            return;
        }

        currentNode = graph.getPathPoint(currentwpindex);

        if (Vector3.Distance(graph.getPathPoint(currentwpindex).transform.position, transform.position) < accuracy) 
        {
            currentwpindex++;
        }

        if (currentwpindex < graph.getPathLength())
        {
            goal = graph.getPathPoint(currentwpindex).transform;
            Vector3 lookatgoal = new Vector3(goal.transform.position.x,
                                           this.transform.position.y,
                                           goal.transform.position.z);

            Vector3 direction = lookatgoal - this.transform.position;
            this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(direction), Time.deltaTime * rotspeed);
            this.transform.Translate(0, 0, speed * Time.deltaTime);
        }
    }

    public void GoToTM()
    {
        graph.AStar(currentNode, wps[0]);
        currentwpindex = 0;
    }

    public void GoToB()
    {
        graph.AStar(currentNode, wps[1]);
        currentwpindex = 0;
    }

    public void GoToCC()
    {
        graph.AStar(currentNode, wps[2]);
        currentwpindex = 0;
    }

    public void GoToORP()
    {
        graph.AStar(currentNode, wps[3]);
        currentwpindex = 0;
    }

    public void GoToT()
    {
        graph.AStar(currentNode, wps[4]);
        currentwpindex = 0;
    }

    public void GoToR()
    {
        graph.AStar(currentNode, wps[5]);
        currentwpindex = 0;
    }

    public void GoToCP()
    {
        graph.AStar(currentNode, wps[6]);
        currentwpindex = 0;
    }

    public void GoToMid()
    {
        graph.AStar(currentNode, wps[7]);
        currentwpindex = 0;
    }
}
