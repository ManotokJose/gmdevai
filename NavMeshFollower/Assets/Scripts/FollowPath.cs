﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FollowPath : MonoBehaviour
{
    public UnityEngine.AI.NavMeshAgent agent;
    public GameObject wpManager;
    GameObject[] wps;

    // Start is called before the first frame update
    void Start()
    {
        wps = wpManager.GetComponent<WaypointManager>().waypoints;
        agent = this.GetComponent<NavMeshAgent>();
    }

    public void GoToTM()
    {
        agent.SetDestination(wps[0].transform.position);
    }

    public void GoToB()
    {
        agent.SetDestination(wps[1].transform.position);
    }

    public void GoToCC()
    {
        agent.SetDestination(wps[2].transform.position);
    }

    public void GoToORP()
    {
        agent.SetDestination(wps[3].transform.position);
    }

    public void GoToT()
    {
        agent.SetDestination(wps[4].transform.position);
    }

    public void GoToR()
    {
        agent.SetDestination(wps[5].transform.position);
    }

    public void GoToCP()
    {
        agent.SetDestination(wps[6].transform.position);
    }

    public void GoToMid()
    {
        agent.SetDestination(wps[7].transform.position);
    }
}
